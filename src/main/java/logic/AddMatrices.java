package main.java.logic;

import java.util.Scanner;

public class AddMatrices {

    // contains two attributes - matrices to be added
    private int[][] firstMatrix;
    private int[][] secondMatrix;

    public void setFirstMatrix(int[][] firstMatrix) {
        this.firstMatrix = firstMatrix;
    }

    public void setSecondMatrix(int[][] secondMatrix) {
        this.secondMatrix = secondMatrix;
    }

    // this method makes the add operation and returns the result
    // keep in mind SOLID - single responsability - one method and one class does one thing
    public int[][] sum() {

        int rows = firstMatrix.length;
        int cols = firstMatrix[0].length;
        int[][] result = new int[rows][cols];

        if (rows == secondMatrix.length && cols == secondMatrix[0].length) {

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    result[i][j] = firstMatrix[i][j] + secondMatrix[i][j];
                }
            }

        } else {
            System.out.println("Sizes not equal !!!");
        }

        return result;
    }
    // BUG: if sizes of matrices are not equal, the method still returns some 0-uri
    //should be solved using Exception
}
