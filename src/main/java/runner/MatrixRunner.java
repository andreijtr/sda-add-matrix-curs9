package main.java.runner;

import main.java.logic.AddMatrices;
import main.java.logic.ToolsMatrix;

public class MatrixRunner {

    public static void main(String[] args) {

        //create objects for adding the matrices and for read / display matrices
        AddMatrices addMatrices = new AddMatrices();
        ToolsMatrix toolsMatrix = new ToolsMatrix();

        //create matrices
        int[][] matrixA = toolsMatrix.readMatrix();
        int[][] matrixB = toolsMatrix.readMatrix();
        int[][] sumOfMatrices;

        addMatrices.setFirstMatrix(matrixA);
        addMatrices.setSecondMatrix(matrixB);

        sumOfMatrices = addMatrices.sum();

        toolsMatrix.showMatrix(sumOfMatrices);
    }
}
