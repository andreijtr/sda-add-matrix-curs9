package test.java;

import main.java.logic.AddMatrices;
import org.junit.Assert;
import org.junit.Test;

public class MatrixTest {

    /*
    - nu s-a putut efectua testarea unitare pe prima varianta (vezi branch master);
    - problema aparea la citirea matricilor, nu puteam introduce date de la tastatura iar programul rula in continuare
    - ca si cum astepta da
    - am creat un branch nou (pe care esti acum) si am exclus citirea matricilor de la tastatura in clasa MAtrix
    - am creat getters si setters si am facut atributele private (incapsulare)
    - in clasa MatrixTest am initializat manual matricile iar testul a trecut
    - am facut un test si cu date gresite care nu a trecut (dupa cum era de asteptat)
     */
    
    @Test
    public void addMatricesTest () {

        AddMatrices addMatrices = new AddMatrices();

        int[][] firstMatrix = {{ 4, 4}, { 4, 4},{ 4, 4}};
        int[][] secondMatrix = {{ 1, 1}, { 1, 1},{ 1, 1}};
        int[][] expected = {{5,5}, {5,5}, {5,5}};

        addMatrices.setFirstMatrix(firstMatrix);
        addMatrices.setSecondMatrix(secondMatrix);

        Assert.assertArrayEquals(expected,addMatrices.sum());
    }

    @Test
    public void addMatricesTestWrong () {

        AddMatrices addMatrices = new AddMatrices();

        int[][] firstMatrix = {{ 4, 4}, { 4, 4},{ 4, 4}};
        int[][] secondMatrix = {{ 1, 1}, { 1, 1},{ 1, 1}};
        int[][] expectedWrong = {{5,6}, {5,5}, {5,5}};

        addMatrices.setFirstMatrix(firstMatrix);
        addMatrices.setSecondMatrix(secondMatrix);

        Assert.assertArrayEquals(expectedWrong, addMatrices.sum());
    }

}
